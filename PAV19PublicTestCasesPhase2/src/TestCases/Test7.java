package TestCases;

public class Test7 {

	
	
	public static void main(String args[]) {
		int y =  inefficientIncrement(10, 0);
	}
	
	public static int inefficientIncrement(int n, int y) {
		int ret;
		if(n <= 0) {
			ret = y+1;
		}else {
			n = n - 1;
			ret = inefficientIncrement(n, y);
		}
		return ret;		
		
	}
}
