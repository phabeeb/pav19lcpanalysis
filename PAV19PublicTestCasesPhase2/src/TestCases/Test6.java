package TestCases;

public class Test6 {	
	
	public static void main(String args[])
	{
		foo();
	}
	public static void foo() {
		int w = 5;
		int y = 2 * w + 6;
		w = bar(y, w);	
		y = w;	
	}
	
	public static int bar(int y, int w) {
		if(w > 0) {
			w = w - 1;
			bar(y, w);
		}
		y = w + 1;
		return w;
	}
}
